package com.company;

/** interfejs dla gry Quiz */
interface Quiz {
    int MIN_VALUE = 0; // minimalny zakres poszukiwan
    int MAX_VALUE = 1000; // maksymalny zakres poszukiwan
    /**
     * metoda rzuca wyjatek ParamTooLarge w przypadku gdy parametr wejsciowy jest wiekszy
     * od oczekiwanej zmiennej, w przeciwnym wypadku rzuca wyjatek ParamTooSmall.
     */
    void isCorrectValue(int value) throws Quiz.ParamTooLarge, Quiz.ParamTooSmall;
    class ParamTooLarge extends Exception {}
    class ParamTooSmall extends Exception {}
}
