package com.company;

/**
 * nalezy zaimplementowac interfejs Quiz.
 */
class QuizImpl implements Quiz{
    private int digit;
    public QuizImpl() {
        this.digit = 254; // w kazdej chwili zmienna moze otrzymac inna wartosc!
    }

    @Override
    public void isCorrectValue(int value) throws ParamTooLarge, ParamTooSmall {
        if (value < digit) {
            throw new ParamTooSmall();
        };
        if (value > digit) {
            throw new ParamTooLarge();
        }
    }
// implementacja metody isCorrectValue...
}

